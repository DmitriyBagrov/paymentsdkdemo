//
//  ViewController.swift
//  paymentsdkdemo
//
//  Created by Багров Дмитрий on 29/06/16.
//  Copyright © 2016 walletOne. All rights reserved.
//

import UIKit
import PaymentSDK
import PayCardsAcquiring

class ViewController: UIViewController {
    
    // MARK: - Private Properties
    
    private let paymentSDKConfiguration = ConfigurationSDK()
    
    // MARK: - Public Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurePaymentSDK()
    }
    
    // MARK: - IBActions
    
    @IBAction func paymentButtonDidTapped(sender: UIButton) {
        PaymentSDKViewController.presentPaymentSDK(self, configuration: paymentSDKConfiguration, delegate: self)
    }
    
    // MARK: - Private Methods
    
    private func configurePaymentSDK() {
        paymentSDKConfiguration.merchantId = 191238281216
        paymentSDKConfiguration.merchantEDSKey = "6957347c416e466d397877614a5b3970574e55367c566278786548"
        
        paymentSDKConfiguration.signatureMethodId = .MD5
        paymentSDKConfiguration.culture = .EN
        
        paymentSDKConfiguration.paymentParams = {
            let paymentParams = PaymentParams()
            paymentParams.generateNewOrderID()
            paymentParams.amount = 12.34
            paymentParams.descriptionPayment = "Test Payment"
            paymentParams.customer = Customer(customerId: "dmitriy.bagrov", firstName: "Dmitriy", lastName: "Bagrov", email: "dmitriy.bagrov@walletone.com", phoneNumber: "9788014244")
            return paymentParams
        }()
    }

}

extension ViewController: PaymentSDKDelegate {
    
    func paymentSDKPaymentComplete(paymentSDKViewController: PaymentSDKViewController, paymentTypeId: PaymentTypeId, paymentStateId: PaymentStateId) {
        print("success")
    }
    
    func paymentSDKPaymentError(paymentSDKViewController: PaymentSDKViewController, paymentTypeId: PaymentTypeId, error: String?) {
        print("error")
    }
    
}


